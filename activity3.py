
"""
[Section] Mini-activity
1.) Create a list of names of 5 students
2.) Create a list of grades for the 5 students
3.) Using a loop, iterate through the list and print "The grade of <student> is <grade>."

"""

student_names = ["Jack", "And", "Jill", "Went", "Up"]
student_grades = [95, 88, 73, 65, 80]
i= 0
for x in range(len(student_names)):
    print("The grade of " + student_names[x] + " is " + str(student_grades[x]) + ".")

# or 

# while i < len(student_names):
#     print("The grade of " + student_names[i] + " is " + str(student_grades[i]) + ".")
#     i += 1

# 1) Create a car dictionary with the following keys: brand, model, year,and color.

# 2) Print the following statement from the details: "I own (color) (brand) (model) and it was made in (year)"

# 3) Create a function that gets the square of a number
# 4) Create a function that takes one of the following languages as its parameter:
# a. French
# b. Spanish
# c  Japanese

#  Depending on which language is given, the function should print "Hello World!" in that language. Add a condition that ask the user input a valid language if the given parameter does not match any of the above.

car = {
    "brand": "Toyota",
    "model": "Civic",
    "year": 2022,
    "color": "red",
}

print(f"I own a {car['color']} {car['brand']} {car['model']} and it was made in {car['year']}.")

def square(num):
    return num * num

print(square(7))

a = "French"
b = "Spanish"
c = "Japanese"

def hello_world(language):
    if language == "French":
        print("Bonjour le monde!")
    elif language == "Spanish":
        print("Hola Mundo!")
    elif language == "Japanese":
        print("こんにちは世界")
    else:
        print("Please enter a valid language")

hello_world(a)
hello_world("Japanese")

